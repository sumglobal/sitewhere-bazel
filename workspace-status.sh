#!/bin/bash

# Bazel executes this script sometime AFTER the load phase

BUILD_TIMESTAMP_ISO=$(date --utc +%Y-%m-%dT%H:%M:%SZ)
BUILD_HASH=$(cat /dev/urandom | head -c16 | md5sum 2>/dev/null | cut -f1 -d' ')
STABLE_GIT_COMMIT=$(git rev-parse HEAD)
STABLE_GIT_BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

# if the name starts with "STABLE_" the output gets written to bazel-out/stable-status.txt
# else it gets written to bazel-out/volatile-status.txt
# Bazel generates and places a BUILD_TIMESTAMP (date +%s) in volatile-status.txt
echo "BUILD_TIMESTAMP_ISO $BUILD_TIMESTAMP_ISO"
echo "BUILD_HASH $BUILD_HASH"
echo "STABLE_GIT_COMMIT $STABLE_GIT_COMMIT"
echo "STABLE_GIT_BRANCH_NAME $STABLE_GIT_BRANCH_NAME"
