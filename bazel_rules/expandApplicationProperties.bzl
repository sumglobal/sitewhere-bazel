load("//:variables.bzl", "BUILD_PROPERTIES", "DEBUG", "VOLATILE_STATUS_FILE", "STABLE_STATUS_FILE")



"""
Outputs the template file with the expanded values from substitutions dictionary
"""
def _propExpand_impl(ctx):
    # Decorate keys with prefix and suffix. e.g. "version" to "@version@"
    # Action will search for decorated keys in template
    _substitutions =  { ctx.attr.prefix+k+ctx.attr.suffix:v for (k,v) in ctx.attr.substitutions.items() }

    print("_substitutions={}".format(_substitutions)) if DEBUG == True else None
    print("template file is: " + ctx.file.template.path) if DEBUG == True else None
    print("output file is: " +  ctx.outputs.output.path) if DEBUG == True else None

    ctx.actions.expand_template(
        template = ctx.file.template,
        output = ctx.outputs.output,
        substitutions = _substitutions,
    )


propExpand = rule(
    implementation = _propExpand_impl,
    attrs = {
        "template": attr.label(
            default = Label("//:pom_template.xml"),
            allow_single_file = True,
            mandatory = True,
        ),
        "output": attr.output(mandatory = True),
        "substitutions": attr.string_dict(
           allow_empty = True,
           mandatory = True,
       ),
       "prefix": attr.string(default = '@'),
       "suffix": attr.string(default = '@'),
    },
)

"""
Outputs the template file with expanded values from volatile-status.txt, stable-status.txt, and substitutions dictionary
"""
def xpand_template( name, template, output, substitutions={}, prefix = '@', suffix = '@'):

    tmp1_output = output + ".tmp1"
    tmp2_output = output + ".tmp2"

    # template_path, volatile_file_path, output_path, prefix, suffix
    native.genrule(
        name = "{}-volatile_substitution".format(name),
        srcs = [template],
        outs = [tmp1_output],
        cmd = "./$(execpath //bazel_rules:template_replace_from_file) $(location {}) {} \"$@\" {} {}".format(
            template, VOLATILE_STATUS_FILE, prefix, suffix),
        tools = ["//bazel_rules:template_replace_from_file"],
        stamp = 1,
    )

    native.genrule(
        name = "{}-stable_substitution".format(name),
        srcs = [tmp1_output],
        outs = [tmp2_output],
        cmd = "./$(execpath //bazel_rules:template_replace_from_file) $(location {}) {} \"$@\" {} {}".format(
            tmp1_output, STABLE_STATUS_FILE, prefix, suffix),
        tools = ["//bazel_rules:template_replace_from_file"],
        stamp = 1,
    )

    #print("len(substitutions)={}".format(len(substitutions))) if DEBUG == True else None
    #if (len(substitutions) > 0):
    propExpand(name=name, template=tmp2_output, output=output, substitutions=substitutions, prefix=prefix, suffix=suffix)



