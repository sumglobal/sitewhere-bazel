load("@bazel_skylib//lib:paths.bzl", "paths")


DEBUG = False

VOLATILE_STATUS_FILE = "bazel-out/volatile-status.txt"
STABLE_STATUS_FILE = "bazel-out/stable-status.txt"

BUILD_PROPERTIES = {
  "version": "2.0.7",
  "pom.group.id": "com.sitewhere",
  "pom.version": "@version@",
  "pom.packaging": "jar",
  "pom.project.url": "http://sitewhere.com",
  "pom.license.name": "Unknown lic",
  "pom.license.url": "http://license_url",
  "pom.scm.connection": "scm:git:git://scm_connection",
  "pom.scm.developer.connection": "scm:git:ssh://scm_developer_connection",
  "pom.scm.tag": "no_tag",
  "pom.scm.url": "http://scm.url",
  "pom.developers": ",".join(["Dev1","Dev2"]),
  "version.identifier": "@version@",
}

# Then, self resolve. Iterate through values. If value starts with @ and ends with @, lookup the key and update the value with the new value

def _resolve_build_properties():
  if DEBUG == True:
    print("Running _resolve_build_properties function!!!!")
  updates = {}
  for key in BUILD_PROPERTIES:
    val = BUILD_PROPERTIES[key]
    if type(val) == "string" and val.startswith("@") and val.endswith("@"):
      new_val = BUILD_PROPERTIES.get(val.strip("@ "),"UNKNOWN")
      updates[key] = new_val
  BUILD_PROPERTIES.update(updates)

_resolve_build_properties()
