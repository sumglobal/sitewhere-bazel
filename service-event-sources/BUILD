load("@bazel_common//tools/maven:pom_file.bzl", "pom_file")
load("//:variables.bzl", "BUILD_PROPERTIES", "DEBUG")

load("@io_bazel_rules_docker//container:container.bzl", "container_image", "container_push")
load("@bazel_tools//tools/build_defs/pkg:pkg.bzl", "pkg_tar")

load("@bazel-springboot-rule//tools/springboot:springboot.bzl", "springboot", "add_web_deps", "add_boot_jetty_starter" ) # once corrected, pull from here instead
#load("//tools/springboot:springboot.bzl", "springboot") # work on the rule locally

_local_build_properties =  {}
_local_build_properties.update(BUILD_PROPERTIES)
# add / override
_local_build_properties.update({
  "bazel.base.name": "service_event_sources",
  "pom.project.name": "service-event-sources",
  "pom.project.description": "service-event-sources desc",
  "pom.artifact.id": "service-event-sources",
})

app_deps = [
               "//sitewhere-communication",
               "//sitewhere-core",
               "//sitewhere-microservice",
               "//sitewhere-grpc-client",
               "//sitewhere-configuration",
               "//sitewhere-core-api",
               "//sitewhere-core-lifecycle",
               "@maven//:com_sitewhere_sitewhere_java_model",
               "@maven//:com_sitewhere_sitewhere_grpc_event_management",
               "@maven//:com_sitewhere_sitewhere_device_protobuf",
               "@maven//:ch_qos_cal10n_cal10n_api",
               "@maven//:org_springframework_boot_spring_boot",
               "@maven//:org_springframework_security_spring_security_core",
               "@maven//:org_springframework_spring_core",
               "@maven//:org_springframework_spring_beans",
               "@maven//:org_springframework_spring_context",
               "@maven//:org_springframework_spring_jcl",
               "@maven//:org_springframework_spring_web",
               "@maven//:org_codehaus_groovy_groovy",
               "@maven//:io_grpc_grpc_stub",
               "@maven//:org_apache_activemq_activemq_amqp",
               "@maven//:org_apache_activemq_activemq_mqtt",
               "@maven//:org_apache_activemq_activemq_stomp",
               "@maven//:org_apache_activemq_activemq_kahadb_store",
               "@maven//:org_apache_activemq_activemq_broker",
               "@maven//:org_apache_activemq_activemq_client",
               "@maven//:javax_websocket_javax_websocket_api",
               "@maven//:com_microsoft_azure_azure_eventhubs",
               "@maven//:com_microsoft_azure_azure_eventhubs_eph",
               "@maven//:com_rabbitmq_amqp_client",
               "@maven//:org_glassfish_tyrus_tyrus_server",
               "@maven//:org_glassfish_tyrus_tyrus_container_grizzly_server",
               "@maven//:org_eclipse_californium_californium_core",
               "@maven//:org_apache_httpcomponents_httpcore",
               "@maven//:org_apache_httpcomponents_httpclient",
               "@maven//:commons_codec_commons_codec",
               "@maven//:commons_lang_commons_lang",
               "@maven//:commons_net_commons_net",
               "@maven//:org_fusesource_hawtdispatch_hawtdispatch",
               "@maven//:org_fusesource_hawtbuf_hawtbuf",
               "@maven//:org_fusesource_mqtt_client_mqtt_client",
               "@maven//:com_fasterxml_jackson_core_jackson_core",
               "@maven//:com_fasterxml_jackson_core_jackson_databind",
               "@maven//:org_slf4j_slf4j_api",
               "@maven//:io_dropwizard_metrics_metrics_core",
               "@maven//:org_apache_geronimo_specs_geronimo_jms_1_1_spec",
       ]


springboot(
        "{}-{}".format(_local_build_properties["pom.artifact.id"], _local_build_properties["pom.version"]),
        "com.sitewhere.sources.EventSourcesApplication",
        deps = app_deps,
)
#add_web_deps(app_deps)
#add_boot_jetty_starter(app_deps)

pkg_tar(
    name = "script_templates",
    strip_prefix = "dockerimage/script-templates",
    package_dir = "/script-templates",
    srcs = ["dockerimage/script-templates"],
    mode = "0755",
)

pkg_tar(
    name = "grpc_health_probe",
    strip_prefix = "",
    package_dir = "/bin",
    srcs = ["@grpc_health_probe//file"],
    mode = "0755",
)
debugCmd = ["java", "-server", "-Xdebug",
                "-Dcom.sun.management.jmxremote.local.only=false",
                "-Dcom.sun.management.jmxremote.ssl=false",
                "-Dcom.sun.management.jmxremote.authenticate=false",
                "-Dcom.sun.management.jmxremote.port=1114",
                "-Dcom.sun.management.jmxremote.rmi.port=1114",
                "-Dcom.sun.management.jmxremote.host=0.0.0.0",
                "-Djava.rmi.server.hostname=0.0.0.0",
                "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8001",
                "-Xmx512M", "-Xss384K", "-jar",
                "-DLOGGING_LEVEL_SITEWHERE=INFO",
                "-DLOGGING_LEVEL_SITEWHERE_GRPC=INFO",
                "-DLOGGING_LEVEL_ZOOKEEPER=ERROR",
                "-DLOGGING_LEVEL_KAFKA=ERROR",
                "/{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"],_local_build_properties["pom.version"])]
cmd = ["java", "-server", "-Xmx512M", "-Xss384K", "-jar",
                 "-DLOGGING_LEVEL_SITEWHERE=INFO",
                 "-DLOGGING_LEVEL_SITEWHERE_GRPC=INFO",
                 "-DLOGGING_LEVEL_ZOOKEEPER=ERROR",
                 "-DLOGGING_LEVEL_KAFKA=ERROR",
                 "/{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"],_local_build_properties["pom.version"])]
container_image(
    name = "image_{}".format(_local_build_properties["bazel.base.name"]),
    labels = {"maintainer" : "admin@sitewhere.com"},
    creation_time = "{BUILD_TIMESTAMP}",
    stamp = True,
    base = "@java_base//image",
    ports = ["8080"] if DEBUG != True else ["8080","8001","1114"],
    files = ["{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"], _local_build_properties["pom.version"])],
    tars = ["//{}:grpc_health_probe".format(_local_build_properties["pom.artifact.id"]),
            "//{}:script_templates".format(_local_build_properties["pom.artifact.id"]),],
    cmd = cmd if DEBUG != True else debugCmd,
)

container_push(
   name = "push_{}".format(_local_build_properties["bazel.base.name"]),
   image = ":image_{}".format(_local_build_properties["bazel.base.name"]),
   format = "Docker",
   registry = "index.docker.io",
   repository = "sumglobal/sitewhere-bazel-{}".format(_local_build_properties["pom.artifact.id"]),
   tag = _local_build_properties['pom.version'] if DEBUG != True else "debug-{}".format(_local_build_properties["pom.version"]),
)
