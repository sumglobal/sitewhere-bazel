workspace(name = "sitewhere")

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")



http_archive(
    name = "bazel_toolchains",
    sha256 = "04b10647f76983c9fb4cc8d6eb763ec90107882818a9c6bef70bdadb0fdf8df9",
    strip_prefix = "bazel-toolchains-1.2.4",
    urls = [
        "https://github.com/bazelbuild/bazel-toolchains/releases/download/1.2.4/bazel-toolchains-1.2.4.tar.gz",
        "https://mirror.bazel.build/github.com/bazelbuild/bazel-toolchains/archive/1.2.4.tar.gz",
    ],
)

load("@bazel_toolchains//rules:rbe_repo.bzl", "rbe_autoconfig")

# Creates a default toolchain config for RBE.
# Use this as is if you are using the rbe_ubuntu16_04 container,
# otherwise refer to RBE docs.
rbe_autoconfig(name = "rbe_default")

# go dependencies
http_archive(
    name = "io_bazel_rules_go",
    urls = [
        "https://storage.googleapis.com/bazel-mirror/github.com/bazelbuild/rules_go/releases/download/v0.20.2/rules_go-v0.20.2.tar.gz",
        "https://github.com/bazelbuild/rules_go/releases/download/v0.20.2/rules_go-v0.20.2.tar.gz",
    ],
    sha256 = "b9aa86ec08a292b97ec4591cf578e020b35f98e12173bbd4a921f84f583aebd9",
)
load("@io_bazel_rules_go//go:deps.bzl", "go_rules_dependencies", "go_register_toolchains","go_download_sdk")

go_download_sdk(
    name = "go_sdk",
    goarch = "amd64",
    goos = "linux",
)
go_download_sdk(
    name = "go_sdk_host",
)
go_rules_dependencies()
go_register_toolchains()

http_archive(
    name = "bazel_gazelle",
    urls = [
        "https://storage.googleapis.com/bazel-mirror/github.com/bazelbuild/bazel-gazelle/releases/download/v0.19.0/bazel-gazelle-v0.19.0.tar.gz",
        "https://github.com/bazelbuild/bazel-gazelle/releases/download/v0.19.0/bazel-gazelle-v0.19.0.tar.gz",
    ],
    sha256 = "41bff2a0b32b02f20c227d234aa25ef3783998e5453f7eade929704dcff7cd4b",
)
load("@bazel_gazelle//:deps.bzl", "gazelle_dependencies", "go_repository")
gazelle_dependencies(go_sdk = "go_sdk_host")

RULES_JVM_EXTERNAL_TAG = "3.0"
RULES_JVM_EXTERNAL_SHA = "62133c125bf4109dfd9d2af64830208356ce4ef8b165a6ef15bbff7460b35c3a"

http_archive(
    name = "rules_jvm_external",
    strip_prefix = "rules_jvm_external-%s" % RULES_JVM_EXTERNAL_TAG,
    sha256 = RULES_JVM_EXTERNAL_SHA,
    url = "https://github.com/bazelbuild/rules_jvm_external/archive/%s.zip" % RULES_JVM_EXTERNAL_TAG,
)

http_archive(
    name = "bazel_common",
    strip_prefix = "bazel-common-f1115e0f777f08c3cdb115526c4e663005bec69b",
    url = "https://github.com/google/bazel-common/archive/f1115e0f777f08c3cdb115526c4e663005bec69b.zip",
)

BAZEL_SKYLIB_TAG = "1.0.2"
BAZEL_SKYLIB_SHA = "97e70364e9249702246c0e9444bccdc4b847bed1eb03c5a3ece4f83dfe6abc44"

http_archive(
    name = "bazel_skylib",
    url = "https://github.com/bazelbuild/bazel-skylib/releases/download/%s/bazel-skylib.%s.tar.gz" %(BAZEL_SKYLIB_TAG, BAZEL_SKYLIB_TAG),
    sha256 = BAZEL_SKYLIB_SHA,
)


load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_file")

http_file(
    name = "grpc_health_probe",
    urls = ["https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.0/grpc_health_probe-linux-amd64"],
    downloaded_file_path = "grpc_health_probe",
    executable = True,
    sha256 = "04983b10ef9e031a11694551bd55e4c9d2b05ddf340e7bf0c4f45ae66caada43",
  )

load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")

new_git_repository(
    name = "sitewhere_swagger",
    remote = "https://github.com/sitewhere/sitewhere2-swagger-ui.git",
    branch = "master",
    build_file_content = """
java_library(
    name='swagger',
#    srcs= glob(['**/*.java']),
    resources=glob(['swagger/**/*']),
    visibility = ["//visibility:public"],
)
    """
)

git_repository(
    name = "rules_python",
    remote = "https://github.com/bazelbuild/rules_python.git",
    commit = "9d68f24659e8ce8b736590ba1e4418af06ec2552",
    shallow_since = "1565801665 -0400"
)

load("@rules_python//python:repositories.bzl", "py_repositories")
py_repositories()

# This one is only needed if you're using the packaging rules.
#load("@rules_python//python:pip.bzl", "pip_repositories")
#pip_repositories()


git_repository(
    name = "bazel-springboot-rule",
    remote = "https://bitbucket.org/sumglobal/bazel-springboot-rule.git",

    commit = "8debc21a3be456d5b8f767498b25965837d85527",
    shallow_since = "1567710870 -0400",
)


android_sdk_repository(
    name = "androidsdk", # Required. Name *must* be "androidsdk".
)


load("@rules_jvm_external//:defs.bzl", "artifact", "maven_install")
load("//:dependencies.bzl", "ALL_DEPS")

maven_install(
    #default name= "maven" which is referenced in the BUILD files
    artifacts = ALL_DEPS,
    excluded_artifacts = [
            "commons-logging:commons-logging", # last straw, can't seem to exclude this from httpcomponents:httpclient
    ],
    version_conflict_policy = "pinned",
    repositories = [
        # Re-published resources to work around issues with "Maven plugin" types in pom files
        #"http://publisher:BQOdT0RB@n3!@sumglobal.asuscomm.com:18081/repository/public/",
        "http://sumglobal.asuscomm.com:18081/repository/public",
        # Private repositories are supported through HTTP Basic auth
        #"http://username:password@localhost:8081/artifactory/my-repository",
        #"https://jcenter.bintray.com/",
        #"https://repo1.maven.org/maven2",
        #"https://repo.maven.apache.org/maven2",
        "https://maven.restlet.org",
        "https://jitpack.io",
        "https://oss.jfrog.org/artifactory/oss-release-local/",
        #"https://oss.sonatype.org/content/repositories/ksoap2-android-releases/",
        "https://repo.spring.io/libs-milestone",
        "https://maven.wso2.org/nexus/content/repositories/releases/",
        "https://oss.sonatype.org/content/repositories/snapshots",


    ],
    fetch_sources = True, # Want this to work happily with our IDE
    generate_compat_repositories = False, # Change to True and uncomment compat_repositories lines below to use alt form alias

)
# List all artifacts retrieved. Each artifact appears to have both a versioned and unversioned alias
#bazel query @maven//:all | sort

# https://github.com/bazelbuild/rules_jvm_external#repository-aliases
#load("@maven//:compat.bzl", "compat_repositories")
#compat_repositories()

# Download the rules_docker repository at release v0.12.0
http_archive(
    name = "io_bazel_rules_docker",
    sha256 = "14ac30773fdb393ddec90e158c9ec7ebb3f8a4fd533ec2abbfd8789ad81a284b",
    strip_prefix = "rules_docker-0.12.1",
    urls = ["https://github.com/bazelbuild/rules_docker/releases/download/v0.12.1/rules_docker-v0.12.1.tar.gz"],
)

# Load the macro that allows you to customize the docker toolchain configuration.
load("@io_bazel_rules_docker//toolchains/docker:toolchain.bzl",
    docker_toolchain_configure="toolchain_configure"
)

docker_toolchain_configure(
  name = "docker_config",
  # Replace this with an absolute path to a directory which has a custom docker
  # client config.json. Note relative paths are not supported.
  # Docker allows you to specify custom authentication credentials
  # in the client configuration JSON file.
  # See https://docs.docker.com/engine/reference/commandline/cli/#configuration-files
  # for more details.
  client_config="/workspace",
)

load(
    "@io_bazel_rules_docker//repositories:repositories.bzl",
    container_repositories = "repositories",
)
container_repositories()

# This is NOT needed when going through the language lang_image
# "repositories" function(s).
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")

container_deps()

# https://github.com/bazelbuild/rules_docker/issues/704
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")

container_pull(
  name = "java_base",
  registry = "index.docker.io",
  repository = "openjdk",
  tag = "8-jre-alpine",
)

load("//:variables.bzl", "BUILD_PROPERTIES", "DEBUG")
print("WORKSPACE load complete...") if DEBUG == True else None

