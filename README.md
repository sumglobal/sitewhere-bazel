

# Bazel

Migrating the Gradle build to Bazel

- WORKSPACE: root directory contains the listings of maven repos and aliases for all the dependent jar files
- Each individual project has a BUILD file which contains the java-library or other primary rule definition

### Bazel Conversion Notes
- Created a dependencies.bzl file to enumerate external dependencies.  Pulled into the WORKSPACE using ALL_DEPS. 
- Additional files to be added to a docker image are bundled with pkg_tar. 
- The `sitewhere-core` subproject puts a timestamp in an `application.properties` file which is stored in the `META-INF` directory
- We purposely chose to keep the granularity of the packages at the module/subproject level.  We realize we could get better performance if we broke the packages down even further
- We are building poms but not publishing them as sitewhere have moved from individual artifact publishing to container publishing
- On the host builds, we are using Azul jdk.  Azul puts it's verison in the manifest jars.  For local caching, this doesn't affect us but it may be a problem when using remote caching
- There are some situations where we have found 1 or 2 extra dependencies in a jar file.  However, we believe this to be correct and cannot explain why Gradle exludes them.
- We forked the repo "salesforce/bazel-springboot-rule".  We updated the code to remove depracated dependency notation.  We also added the ability to use a `libswagger.jar` file to add swagger components


### Summary
There are 11 libraries and 19 microservices.  Each of them has a gradle.build file and need to be converted along with all the proper rules to match the gradle tasks
For the initial migration, this granularity will be kept. 
In each location where a build.gradle file located, a BUILD file will be added to build the same rules for the tasks in
the gradle.build file.  The BUILD file will contain the sources and dependencies
needed.  Migration will be done on the libraries first and then the microservices.
They should be migrated in the order of dependencies.


##### When tracking down dependencies, start with the module's dependencies as defined by Gradle (and Spring dependency plugin)
For example:

`cd sitewhere-core`

`gradle -q dependencies`

Gradle (spring dependency plugin) is using a BOM as the primary source to determine artifact versions:
https://repo1.maven.org/maven2/io/spring/platform/platform-bom/Cairo-SR1/platform-bom-Cairo-SR1.pom
which also includes:
https://repo1.maven.org/maven2/org/springframework/boot/spring-boot-starter-parent/2.0.2.RELEASE/spring-boot-starter-parent-2.0.2.RELEASE.pom
https://repo1.maven.org/maven2/org/springframework/boot/spring-boot-dependencies/2.0.2.RELEASE/spring-boot-dependencies-2.0.2.RELEASE.pom
These each include BOMs as well, like jackson-bom. So, the gradle -q dependencies is the way to go....

Behind the scenes Bazel (maven_install rule) uses Coursier to pull direct and resolve & fetch transitive dependencies

e.g. `coursier resolve -t com.google.guava:guava:20.0`

You can also use Bazel query to see the dependencies for a target:
(Find dependencies being fetched (from @maven repo))
e.g. `bazel query 'deps(//service-device-registration:service-device-registration-2.0.7-boot.jar)' --output=graph | grep @maven//`

## Progress

### Libraries
- Migrated  sitewhere-core-api to build command line is: `bazel build //sitewhere-core-api`
    - status: COMPLETE
- Migrated sitewhere-core-lifecycle to build command line is: `bazel build //sitewhere-core-lifecycle`
    - this also builds the sitewhere-core-api as its a dependency
    - status: COMPLETE
- Migrated sitewhere-core to build command line is: `bazel build //sitewhere-core`
    - this also builds the previous 2 libraries if they are not built
    - NEEDS to be able to do variable substitution for the properties files. DONE
    - status: COMPLETE
- Migrated sitewhere-configuration to build command line is: `bazel build //sitewhere-configuration`
    - this also builds the sitewhere-core-api and sitewhere-core,if needed, as both are dependencies
    - status: COMPLETE
- Migrated sitewhere-influxdb to build command line is: `bazel build //sitewhere-influxdb`
    - this also builds the sitewhere-core-api, sitewhere-core-lifecycle, and sitewhere-configuration, as dependencies
    - status: COMPLETE
- Migrated sitewhere-cassandra to build command line is: `bazel build //sitewhere-cassandra`
    - this also builds the sitewhere-core-api, sitewhere-core-lifecycle, and sitewhere-configuration, as dependencies
    - status: COMPLETE
- Migrated sitewhere-solr to build command line is: `bazel build //sitewhere-solr`
    - this also builds the sitewhere-core-api, sitewhere-core-lifecycle, and sitewhere-configuration, as dependencies
    - status: COMPLETE
- Migrated sitewhere-mongodb to build command line is: `bazel build //sitewhere-mongodb`
    - this also builds the sitewhere-core-api, sitewhere-core-lifecycle, and sitewhere-configuration, as dependencies
    - status: COMPLETE
- Migrated sitewhere-communication to build command line is: `bazel build //sitewhere-communication`
    - this also builds the sitewhere-core-api and sitewhere-core-lifecycle as dependencies
    - Modified COAP_SERVER in src/test/java/com/sitewhere/communication/test/coap/CoapTests.java to point to "localhost"
    - status: COMPLETE
##### The sitewhere-communication library tests use MQTT and CoAP test servers.   These servers MUST be up for tests to pass:
`
    docker run --name mqtt-test -dit
        -p 1883:1883 -p 9001:9001
       toke/mosquitto
`
Test command line is: `bazel test //sitewhere-communication:MqttTests`
`
    docker run --name coap-test -dit -p 8583:5683/udp aleravat/coap-test-server:latest
`
Test command line is: `bazel test //sitewhere-communication:CoapTests`

- Migrated sitewhere-grpc-client to build command line is: `bazel build //sitewhere-grpc-client`
    - this also builds the sitewhere-core, sitewhere-core-api, and sitewhere-core-lifecycle as dependencies
    - status: COMPLETE
- Migrated sitewhere-microservice to build command line is: `bazel build //sitewhere-microservice`
    - this also builds the sitewhere-core, sitewhere-core-api, and sitewhere-core-lifecycle as dependencies
    - Bazel requires Android build tools version 26.0.1 or newer for this build
    - Export ANDROID_HOME to /usr/local/share/android-sdk
    - status: COMPLETE

### Service migration
All of the services are spring boot. We are using a modified version of the Salesforce springboot rule. It defines a collection of default dependencies.
For this to work, they must be defined in the workspace, or in our case the `dependencies.bzl` file within this file their are comments around the defaults that this rule includes.
Search in that file for: `# == START bazel-springboot-rule defaults == `

##### The microservices use opentracing.io (jaeger) for instrumentation / distributed tracing. The tracing service MUST be up before attempting to bring up a service:
`
docker run --name jaeger -d -p 5775:5775/udp -p 6831:6831/udp -p 6832:6832/udp -p 5778:5578 -p 16686:16686 -p 14268:14268 jaegertracing/all-in-one:latest
`    
##### Lastly, on MacOS, add jaeger as one of the aliases for localhost
    
##### Healthcheck TODO
https://github.com/grpc-ecosystem/grpc-health-probe

If you have GO installed:
`go get github.com/grpc-ecosystem/grpc-health-probe`

`grpc-health-probe --help`

##### Comparison notes
Gradle-built and Bazel-built jars were compared with DiffMerge (https://sourcegear.com/diffmerge/)
The libraries have different names between Gradle and Bazel (sitewhere-core-2.0.7.jar versus libsitewhere-core.jar)
When the jars are compared, many classes show to be different. This appears to be attributed to JDK (Oracle vs Azul) or JDK versions
A binary file compare (https://ridiculousfish.com/hexfiend/) was unable to confirm this however.
A sampling of jars with conflicts were decompiled with CFR (http://www.benf.org/other/cfr/)
  - In general, the decompiled sources matched. There were a few places where the decompiler optimized the source in one case but not the other
  
## To get the minimal install done need to port the following services first
- Asset Management  -  COMPLETED BOOT JAR `bazel build //service-asset-management:service-asset-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-asset-management/service-asset-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_asset_management`
- Device Management  - COMPLETED BOOT JAR `bazel build //service-device-management:service-device-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-device-management/service-device-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_device_management`
- Event Management - COMPLETED BOOT JAR `bazel build //service-event-management:service-event-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-event-management/service-event-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_event_management`
- Event Sources - COMPLETED BOOT JAR `bazel build //service-event-sources:service-event-sources-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGNG_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-event-sources/service-event-sources-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_event_sources`
- Inbound Processing - COMPLETED BOOT JAR bazel build //service-inbound-processing:service-inbound-processing-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-inbound-processing/service-inbound-processing-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_inbound_processing` 
- Instance Management - COMPLETED BOOT JAR `bazel build //service-event-management:service-instance-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-instance-management/service-instance-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_instance_management`
- Outbound Connectors - COMPLETED BOOT JAR `bazel build //service-event-management:service-outbound-connectors-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-outbound-connectors/service-outbound-connectors-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_outbound_connectors`
- Web Rest - COMPLETED BOOT WAR `bazel build //service-event-management:service-web-rest-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-web-rest/service-web-rest-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_web_rest`


NOTE this may not be enough, we may need to up the source to 2.1.0 in order to have a working instance based on the helm charts available

#### Service Migration Progress
- Device Registration  - COMPLETED BOOT JAR `bazel build //service-device-registration:service-device-registration-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-device-registration/service-device-registration-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_device_registration`
    - Run the healthcheck `grpc-health-probe ??????`
- Batch Operations  - COMPLETED BOOT JAR `bazel build //service-batch-operations:service-batch-operations-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-batch-operations/service-batch-operations-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_batch_operations`
- Command Delievery  - COMPLETED BOOT JAR `bazel build //service-command-delivery:service-command-delivery-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-command-delivery/service-command-delivery-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_command_delivery`
- Device State  - COMPLETED BOOT JAR `bazel build //service-device-state:service-device-state-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-device-state/service-device-state-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_device_state`
- Event Search  - COMPLETED BOOT JAR `bazel build //service-event-search:service-event-search-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-event-search/service-event-search-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_event_search`
- Label Generation  - COMPLETED BOOT JAR `bazel build //service-label-generation:service-label-generation-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-label-generation/service-label-generation-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_label_generation`
- Rule Processing  - COMPLETED BOOT JAR `bazel build //service-rule-processing:service-rule-processing-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-rule-processing/service-rule-processing-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_rule_processing`
- Schedule Management  - COMPLETED BOOT JAR `bazel build //service-schedule-management:service-schedule-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-schedule-management/service-schedule-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_schedule_managment`
- Streaming Media  - COMPLETED BOOT JAR `bazel build //service-streaming-media:service-streaming-media-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-streaming-media/service-streaming-media-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_streaming_media`
- Tenant Management  - COMPLETED BOOT JAR `bazel build //service-tenant-management:service-tenant-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-tenant-management/service-tenant-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_tenant_management`
- User Management  - COMPLETED BOOT JAR `bazel build //service-user-management:service-user-management-2.0.7`
    - test spring boot: `java -jar -DLOGGING_LEVEL_SITEWHERE=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_SITEWHERE_GRPC=INFO -DLOGGING_LEVEL_ZOOKEEPER=ERROR -DLOGGING_LEVEL_KAFKA=ERROR bazel-bin/service-user-management/service-user-management-2.0.7-boot.jar`
    - build docker image and push to docker hub: `bazel run push_service_user_management`


# ORIGINAL README BELOW

[![Build Status](https://travis-ci.org/sitewhere/sitewhere.svg?branch=master)](https://travis-ci.org/sitewhere/sitewhere)

![SiteWhere](https://s3.amazonaws.com/sitewhere-branding/SiteWhereLogo.svg)

---

SiteWhere is an industrial-strength open source IoT Application Enablement Platform 
that facilitates the ingestion, storage, processing, and integration of device data 
at massive scale. The platform has been designed from the ground up to take advantage of the latest
technologies in order to scale efficiently to the loads expected in large IoT
projects. 

![SiteWhere Electron App](https://s3.amazonaws.com/sitewhere-web/github-readme/vue-user-interface.png "SiteWhere Electron Application")

SiteWhere embraces a completely distributed architecture using [Kubernetes](https://kubernetes.io/)
as the infrastructure and a variety of microservices to build out the system.
This approach allows customization and scaling at a fine-grained level
so that the system may be tailored to many potential IoT use cases. SiteWhere
is built with a framework approach using clearly defined APIs so that new
technologies may easily be integrated as the IoT ecosystem evolves.


## Kubernetes

SiteWhere is composed of Java-based microservices which are built as
[Docker](https://www.docker.com/) images and deployed to Kubernetes for
orchestration. To simplify deployement, [Helm](https://helm.sh/) is used to
provide standard templates for various deployment scenarios. Helm
[charts](https://github.com/sitewhere/sitewhere-recipes/tree/master/charts)
are provided which supply all of the dependencies needed to run a complete
SiteWhere instance, including both the microservices and infrastructure
components such as Apache Zookeeper, Kafka, Mosquitto MQTT broker,
and other supporting technologies.

## Microservices

SiteWhere 2.0 introduces a much different architectural approach than was used
in the 1.x platform. While the core APIs are mostly unchanged, the system implementation
has moved from a monolithic structure to one based on microservices. This approach
provides a number of advantages over the previous architecture.

![SiteWhere Architecture](http://sitewhere.io/docs/en/2.0.EA1/_images/microservices-diagram.png "SiteWhere 2.0 Architecture")

### Separation of Concerns

Each microservice is a completely self-contained entity that has its
own configuration schema, internal components, data persistence, and
interactions with the event processing pipeline. SiteWhere microservices
are built on top of a custom microservice framework and run as separate
[Spring Boot](https://projects.spring.io/spring-boot/) processes, each
contained in its own [Docker](https://www.docker.com/) image.

Separating the system logic into microservices allows the interactions
between various areas of the system to be more clearly defined. This
transition has resulted in a more understandable and maintainable
system and should continue to pay dividends as more features are added.

### Scale What You Need. Leave Out What You Don't

The microservice architecture allows individual functional areas of the system to be scaled
independently or left out completely. In use cases where REST processing tends to
be a bottleneck, multiple REST microservices can be run concurrently to handle the load.
Conversely, services such as presence management that may not be required can be left
out so that processing power can be dedicated to other aspects of the system.

## Instance Management

The 2.0 architecture introduces the concept of a SiteWhere _instance_, which
allows the distributed system to act as a cohesive unit with some aspects
addressed at the global level. All of the microservices for a single SiteWhere
instance must be running on the same Kubernetes infrastucture, though the system
may be spread across tens or hundreds of machines to distribute the processing
load.

### Centralized Configuration Management with Apache ZooKeeper

SiteWhere 2.0 moves system configuration from the filesystem into
[Apache ZooKeeper](https://zookeeper.apache.org/) to allow for a centralized
approach to configuration management. ZooKeeper contains a hierarchical structure
which represents the configuration for one or more SiteWhere instances
and all of the microservices that are used to realize them.

Each microservice has a direct connection to ZooKeeper and uses the
hierarchy to determine its configuration at runtime. Microservices listen for changes
to the configuration data and react dynamically to updates. No configuration
is stored locally within the microservice, which prevents problems with
keeping services in sync as system configuration is updated.

### Distributed Storage with Rook.io

Since many of the system components such as Zookeeper, Kafka, and various
databases require access to persistent storage, SiteWhere 2.0 uses
[Rook.io](https://rook.io/) within Kubernetes to supply distributed,
replicated block storage that is resilient to hardware failures while
still offering good performance characteristics. As storage and throughput
needs increase over time, new storage devices can be made available
dynamically. The underlying [Ceph](https://ceph.com/) architecture
used by Rook.io can handle _exobytes_ of data while allowing data
to be resilient to failures at the node, rack, or even datacenter level.

### Service Discovery with HashiCorp Consul

With the dynamic nature of the microservices architecture, it is imporant
for microservices to be able to efficiently locate running instances of
the various other services they interact with. SiteWhere 2.0 leverages
[Consul](https://www.consul.io/) for service discovery. Each microservice
registers with Consul and provides a steady stream of updates to the
(potentially replicated) central store. As instances of microservices are
added or removed, SiteWhere dynamically adjusts connectivity to take
advantage of the available resources.

## High Performance Data Processing Pipeline

The event processing pipeline in SiteWhere 2.0 has been completely redesigned and uses
[Apache Kafka](https://kafka.apache.org/) to provide a resilient, high-performance
mechanism for progressively processing device event data. Microservices can plug in to
key points in the event processing pipeline, reading data from well-known inbound topics,
processing data, then sending data to well-known outbound topics. External entites that
are interested in data at any point in the pipeline can act as consumers of the SiteWhere
topics to use the data as it moves through the system.

### Fully Asynchronous Pipeline Processing

In the SiteWhere 1.x architecture, the pipeline for outbound processing used a blocking
approach which meant that any single outbound processor could block the outbound pipeline.
In SiteWhere 2.0, each outbound connector is a true Kafka consumer with its own offset
marker into the event stream. This mechanism allows for outbound processors to process data
at their own pace without slowing down other processors. It also allows services to
leverage Kafka's consumer groups to distribute load across multiple consumers and
scale processing accordingly.

Using Kafka also has other advantages that are leveraged by SiteWhere. Since all data for
the distributed log is stored on disk, it is possible to "replay" the event stream based
on previously gathered data. This is extremely valuable for aspects such as debugging
processing logic or load testing the system.

## Persistent API Connectivity Between Microservices

While device event data generally flows in a pipeline from microservice to microservice on
Kafka topics, there are also API operations that need to occur in real time between the
microservices. For instance, device management and event management functions are contained in
separate microservices, so as new events come in to the system, the inbound processing microservice
needs to interact with device management to look up existing devices in the system and event
management in order to persist the events to a datastore such as
[Apache Cassandra](http://cassandra.apache.org/).

### Using gRPC for a Performance Boost

Rather than solely using REST services based on HTTP 1.x, which tend to have significant
connection overhead, SiteWhere 2.0 uses [gRPC](https://grpc.io/) to establish a long-lived
connection between microservices that need to communicate with each other. Since gRPC uses
persistent HTTP2 connections, the overhead for interactions is greatly reduced, allowing
for decoupling without a significant performance penalty.

The entire SiteWhere data model has been captured in
[Google Protocol Buffers](https://developers.google.com/protocol-buffers/) format so that
it can be used within GRPC services. All of the SiteWhere APIs are now exposed directly as
gRPC services as well, allowing for high-performance, low-latency access to what was previously
only accessible via REST. The REST APIs are still made available via the Web/REST microservice,
but they use the gRPC APIs underneath to provide a consistent approach to accessing data.

Since the number of instances of a given microservice can change over time as the service is
scaled up or down, SiteWhere automatically handles the process of connecting/disconnecting the
gRPC pipes between microservices. Each outbound gRPC client is demulitplexed across the pool
of services that can satisfy the requests, allowing the requests to be processed in parallel.

## Distributed Multitenancy

The SiteWhere 1.x approach to multitenancy was to use a separate "tenant engine" for each tenant.
The engine supported all tenant-specific tasks such as data persistence, event processing, etc.
Since SiteWhere 2.0 has moved to a microservices architecture, the multitenant model has been
distributed as well. SiteWhere supports two types of microservices: global and multitenant.

### Global Microservices

Global microservices do not handle tenant-specific tasks. These services handle aspects such
as instance-wide user management and tenant management that are not specific to individual
system tenants. The Web/REST microservice that supports the REST services and Swagger user
interface is also a global service, since supporting a separate web container for each tenant
would be cumbersome and would break existing SiteWhere 1.x applications. There is also a
global instance management microservice that monitors various aspects of the entire instance
and reports updates to the individual microservces via Kafka.

### Multitenant Microservices

Most of the SiteWhere 2.0 services are multitenant microservices which delegate traffic
to tenant engines that do the actual processing. For instance, the inbound processing microservice
actually consists of many inbound processing tenant engines, each of which is configured separately
and can be started/stopped/reconfigured without affecting the other tenant engines.

The new approach to tenant engines changes the dynamics of SiteWhere event processing. It is now
possible to stop a single tenant engine without the need for stopping tenant engines running in
other microservices. For instance, inbound processing for a tenant can be stopped
and reconfigured while the rest of the tenant pipeline continues processing. Since new
events can be allowed to stack up in Kafka, the tenant engine can be stopped, reconfigured,
and restarted, then resume where it left off with no data loss.


* * * *

Copyright (c) 2009-2018 [SiteWhere LLC](http://www.sitewhere.com). All rights reserved.
