load("@bazel_common//tools/maven:pom_file.bzl", "pom_file")
load("//:variables.bzl", "BUILD_PROPERTIES", "DEBUG")
load("@io_bazel_rules_docker//container:container.bzl", "container_image", "container_push")
load("@bazel_tools//tools/build_defs/pkg:pkg.bzl", "pkg_tar")
load("@bazel-springboot-rule//tools/springboot:springboot.bzl", "springboot") 

_local_build_properties =  {}
_local_build_properties.update(BUILD_PROPERTIES)
# add / override
_local_build_properties.update({
  "bazel.base.name": "service_web_rest",
  "pom.project.name": "service-web-rest",
  "pom.project.description": "service-web-rest desc",
  "pom.artifact.id": "service-web-rest",
})

app_deps = [
    "//sitewhere-communication",
    "//sitewhere-configuration",
    "//sitewhere-core",
    "//sitewhere-core-api",
    "//sitewhere-core-lifecycle",
    "//sitewhere-grpc-client",
    "//sitewhere-microservice",
    "@sitewhere_swagger//:swagger",
    "@maven//:ch_qos_cal10n_cal10n_api",
    "@maven//:com_fasterxml_jackson_core_jackson_annotations",
    "@maven//:com_fasterxml_jackson_core_jackson_databind",
    "@maven//:com_sitewhere_sitewhere_java_model",
    "@maven//:commons_lang_commons_lang",
    "@maven//:io_jsonwebtoken_jjwt",
    "@maven//:io_opentracing_contrib_opentracing_spring_web",
    "@maven//:io_opentracing_contrib_opentracing_web_servlet_filter",
    "@maven//:io_springfox_springfox_core",
    "@maven//:io_springfox_springfox_spring_web",
    "@maven//:io_springfox_springfox_spi",
    "@maven//:io_springfox_springfox_swagger2",
    "@maven//:io_swagger_swagger_annotations",
    "@maven//:org_apache_tomcat_embed_tomcat_embed_core",
    "@maven//:org_apache_tomcat_embed_tomcat_embed_websocket",
    "@maven//:org_springframework_security_spring_security_config",
    "@maven//:org_springframework_security_spring_security_core",
    "@maven//:org_springframework_security_spring_security_web",
    "@maven//:org_springframework_spring_jcl",
    "@maven//:org_springframework_spring_messaging",
    "@maven//:org_springframework_spring_web",
    "@maven//:org_springframework_spring_webmvc",
    "@maven//:org_springframework_spring_websocket",
    "@maven//:org_springframework_boot_spring_boot_starter_web",
    "@maven//:org_springframework_boot_spring_boot_starter_json",
    "@maven//:org_springframework_boot_spring_boot_starter_tomcat",
    "@maven//:org_springframework_boot_spring_boot_starter_websocket"

]

springboot(
        name = "{}-{}".format(_local_build_properties["pom.artifact.id"], _local_build_properties["pom.version"]),
        boot_app_class = "com.sitewhere.web.WebRestApplication",
        deps = app_deps,
)


pkg_tar(
    name = "grpc_health_probe",
    strip_prefix = "",
    package_dir = "/bin",
    srcs = ["@grpc_health_probe//file"],
    mode = "0755",
)

debugCmd = ["java", "-server", "-Xdebug",
                "-Dcom.sun.management.jmxremote.local.only=false",
                "-Dcom.sun.management.jmxremote.ssl=false",
                "-Dcom.sun.management.jmxremote.authenticate=false",
                "-Dcom.sun.management.jmxremote.port=1110",
                "-Dcom.sun.management.jmxremote.rmi.port=1110",
                "-Dcom.sun.management.jmxremote.host=0.0.0.0",
                "-Djava.rmi.server.hostname=0.0.0.0",
                "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8001",
                "-Xmx512M", "-Xss384K", "-jar",
                "-DLOGGING_LEVEL_SITEWHERE=INFO",
                "-DLOGGING_LEVEL_SITEWHERE_GRPC=INFO",
                "-DLOGGING_LEVEL_ZOOKEEPER=ERROR",
                "-DLOGGING_LEVEL_KAFKA=ERROR",
                "/{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"],_local_build_properties["pom.version"])]
cmd = ["java", "-server", "-Xmx512M", "-Xss384K", "-jar",
                 "-DLOGGING_LEVEL_SITEWHERE=INFO",
                 "-DLOGGING_LEVEL_SITEWHERE_GRPC=INFO",
                 "-DLOGGING_LEVEL_ZOOKEEPER=ERROR",
                 "-DLOGGING_LEVEL_KAFKA=ERROR",
                 "/{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"],_local_build_properties["pom.version"])]
container_image(
    name = "image_{}".format(_local_build_properties["bazel.base.name"]),
    labels = {"maintainer" : "admin@sitewhere.com"},
    creation_time = "{BUILD_TIMESTAMP}",
    stamp = True,
    base = "@java_base//image",
    ports = ["8080"] if DEBUG != True else ["8080","8001","1110"],
    files = ["{}-{}-boot.jar".format(_local_build_properties["pom.artifact.id"], _local_build_properties["pom.version"])],
    tars = ["//{}:grpc_health_probe".format(_local_build_properties["pom.artifact.id"])],
    cmd = cmd if DEBUG != True else debugCmd,
)

container_push(
   name = "push_{}".format(_local_build_properties["bazel.base.name"]),
   image = ":image_{}".format(_local_build_properties["bazel.base.name"]),
   format = "Docker",
   registry = "index.docker.io",
   repository = "sumglobal/sitewhere-bazel-{}".format(_local_build_properties["pom.artifact.id"]),
   tag = _local_build_properties['pom.version'] if DEBUG != True else "debug-{}".format(_local_build_properties["pom.version"]),
)



