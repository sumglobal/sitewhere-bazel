import sys

"""
Replace keys in template with values from a keyvalue file

Parameters:
    template_path: Source template file
    keyvalue_path: each line should have a key and a value separated by a space like "BUILD_TIMESTAMP 123456789"
    output_path: Output file 
    prefix: Key prefix. Indicates a value to be replaced in template
    suffix: Key suffix. Indicates a value to be replaced in template
    
Each key is decorated (wrapped) with the prefix and suffix. The code searches for the decorated key in the template
"""
_, template_path, keyvalue_path, output_path, prefix, suffix = sys.argv

with open(template_path, 'r') as template_file, \
        open(keyvalue_path, 'r') as keyvalue_file, \
        open(output_path, 'w') as output_file:

    template = template_file.read().strip()
    tmpl = template

    for line in keyvalue_file:
        kv = line.split()
        if len(kv) == 0:
            continue
        if len(kv) == 1:
            kv.append("")
        delimitedKey = "{}{}{}".format(prefix,kv[0],suffix)
        #print delimitedKey
        tmpl = tmpl.replace(delimitedKey, kv[1])

    output_file.write(tmpl)
