load("@rules_jvm_external//:specs.bzl", "maven")

_APACHE_CURATOR_VER="2.12.0"
_APACHE_TOMCAT_VER="8.5.28"
_IO_NETTY_VER="4.1.27.Final"
_JACKSON_VER="2.9.4"
_SITEWHERE_EXT_VER="2.0.3"
_SPRING_BOOT_VER="2.0.0.RELEASE"
_SPRING_FRM_VER="5.0.4.RELEASE"
_SPRINGFOX_VER="2.9.2"
_GRPC_VER="1.16.1"

_DEPS = [
    "ch.qos.cal10n:cal10n-api:0.7.7",
    "ch.qos.logback:logback-classic:1.2.3",
    "ch.qos.logback:logback-core:1.2.3",
    "com.amazonaws:aws-java-sdk-core:1.9.6",
    "com.amazonaws:aws-java-sdk-sqs:1.9.6",
    "com.brsanthu:google-analytics-java:2.0.0",
    #maven.artifact(
    #        group = "com.brsanthu",
    #        artifact = "google-analytics-java",
    #        version = "2.0.0",
    #        exclusions = [
    #                "org.apache.httpcomponents:httpclient",
    #        ]
    #),
    "com.datastax.cassandra:cassandra-driver-core:3.4.0",
    "com.fasterxml.jackson.core:jackson-annotations:2.9.0",
    "com.fasterxml.jackson.core:jackson-core:%s" % _JACKSON_VER,
    "com.fasterxml.jackson.core:jackson-databind:%s" % _JACKSON_VER,
    "com.fasterxml.jackson.datatype:jackson-datatype-guava:%s" % _JACKSON_VER,
    "com.fasterxml.jackson.datatype:jackson-datatype-jdk8:%s" % _JACKSON_VER,
    "com.google.api.grpc:proto-google-common-protos:1.0.0",
    "com.google.code.findbugs:jsr305:3.0.2",
    "com.google.code.gson:gson:2.8.2",
    "com.google.errorprone:error_prone_annotations:2.2.0",
    "com.google.guava:guava:20.0",
    "com.google.protobuf:protobuf-java:3.5.1",
    "com.orbitz.consul:consul-client:1.2.3",
    "com.rabbitmq:amqp-client:3.6.0",
    "com.sitewhere:sitewhere-grpc-asset-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-batch-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-common:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-device-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-device-state:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-event-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-label-generation:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-schedule-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-stream-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-tenant-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-grpc-user-management:%s" % _SITEWHERE_EXT_VER,
    "com.sitewhere:sitewhere-device-protobuf:2.0.1",
    "com.sitewhere:sitewhere-java-model:2.0.2",
    "com.squareup.okhttp3:okhttp:3.10.0",
    "com.squareup.okio:okio:1.14.0",
    "com.squareup.retrofit2:converter-jackson:2.3.0",
    "com.squareup.retrofit2:retrofit:2.3.0",
    "com.uber.jaeger:jaeger-core:0.21.0",
    "com.uber.jaeger:jaeger-thrift:0.21.0",
    "com.vaadin.external.google:android-json:0.0.20131108.vaadin1",
    "com.vividsolutions:jts:1.13",
    "commons-codec:commons-codec:1.11",
    "commons-collections:commons-collections:3.2.2",
    "commons-io:commons-io:2.6",
    "commons-lang:commons-lang:2.6",
    "commons-net:commons-net:3.6",
    "io.dropwizard.metrics:metrics-core:3.2.6",
    "io.grpc:grpc-context:%s" % _GRPC_VER,
    "io.grpc:grpc-core:%s" % _GRPC_VER,
    "io.grpc:grpc-netty:%s" % _GRPC_VER,
    "io.grpc:grpc-protobuf-lite:%s" % _GRPC_VER,
    "io.grpc:grpc-protobuf:%s" % _GRPC_VER,
    "io.grpc:grpc-stub:%s" % _GRPC_VER,
    "io.jsonwebtoken:jjwt:0.7.0",
    "io.netty:netty-buffer:%s" % _IO_NETTY_VER,
    "io.netty:netty-codec-http2:%s" % _IO_NETTY_VER,
    "io.netty:netty-codec-http:%s" % _IO_NETTY_VER,
    "io.netty:netty-codec-socks:%s" % _IO_NETTY_VER,
    "io.netty:netty-codec:%s" % _IO_NETTY_VER,
    "io.netty:netty-common:%s" % _IO_NETTY_VER,
    "io.netty:netty-handler-proxy:%s" % _IO_NETTY_VER,
    "io.netty:netty-handler:%s" % _IO_NETTY_VER,
    "io.netty:netty-resolver:%s" % _IO_NETTY_VER,
    "io.netty:netty-transport:%s" % _IO_NETTY_VER,
    "io.opencensus:opencensus-api:0.12.3",
    "io.opencensus:opencensus-contrib-grpc-metrics:0.12.3",
    "io.opentracing:opentracing-api:0.30.0",
    "io.opentracing.contrib:opentracing-spring-web:0.0.10",
    "io.opentracing.contrib:opentracing-web-servlet-filter:0.0.9",
    "io.opentracing:opentracing-noop:0.30.0",
    "io.opentracing:opentracing-util:0.30.0",
    "io.springfox:springfox-core:%s" % _SPRINGFOX_VER,
    "io.springfox:springfox-spi:%s" % _SPRINGFOX_VER,
    "io.springfox:springfox-spring-web:%s" % _SPRINGFOX_VER,
    "io.springfox:springfox-swagger2:%s" % _SPRINGFOX_VER,
    "io.swagger:swagger-annotations:1.5.21",
    "javax.jms:jms:1.1",
    "javax.annotation:javax.annotation-api:1.3.2",
    "jline:jline:2.14.6",
    "joda-time:joda-time:2.9.9",
    "junit:junit:4.12", # test
    "org.apache.commons:commons-lang3:3.7",
    "org.apache.commons:commons-math3:3.4.1",
    maven.artifact(
            group = "org.apache.curator",
            artifact = "curator-framework",
            version = _APACHE_CURATOR_VER,
            exclusions = [
                    "log4j:log4j",
                    "org.slf4j:slf4j-log4j12",
                    "io.netty:netty",
            ]
        ),
    maven.artifact(
            group = "org.apache.curator",
            artifact = "curator-recipes",
            version = _APACHE_CURATOR_VER,
            exclusions = [
                    "log4j:log4j",
                    "org.slf4j:slf4j-log4j12",
                    "io.netty:netty",
            ]
        ),
    maven.artifact(
            group = "org.apache.curator",
            artifact = "curator-client",
            version = _APACHE_CURATOR_VER,
            exclusions = [
                    "log4j:log4j",
                    "org.slf4j:slf4j-log4j12",
                    "io.netty:netty",
            ]
        ),
    maven.artifact(
            group = "org.apache.httpcomponents",
            artifact = "httpclient",
            version = "4.5.5",
            exclusions = [
                    "commons-logging:commons-logging",
            ]
    ),
    "org.apache.httpcomponents:httpcore:4.4.9",
    "org.apache.httpcomponents:httpclient:4.5.5",
    "org.apache.httpcomponents:httpmime:4.5.5",
    "org.apache.kafka:kafka-clients:2.0.0",
    "org.apache.logging.log4j:log4j-api:2.10.0",
    "org.apache.logging.log4j:log4j-to-slf4j:2.10.0",
    "org.apache.solr:solr-solrj:6.6.3",
    "org.apache.thrift:libthrift:0.9.2",
    "org.apache.tomcat.embed:tomcat-embed-core:%s" % _APACHE_TOMCAT_VER,
    "org.apache.tomcat.embed:tomcat-embed-websocket:%s" % _APACHE_TOMCAT_VER,
    "org.apache.zookeeper:zookeeper:3.4.8",
    maven.artifact(
        group = "org.apache.zookeeper",
        artifact = "zookeeper",
        version = "3.4.8",
        exclusions = [
                "log4j:log4j",
                "org.slf4j:slf4j-log4j12",
                "io.netty:netty",
        ]
    ),
    "org.codehaus.groovy:groovy:2.4.12",
    "org.codehaus.mojo:animal-sniffer-annotations:1.17",
 #  "org.codehaus.woodstox:stax2-api:3.1.4",
 #  "org.codehaus.woodstox:woodstox-core-asl:4.4.1",
    "org.eclipse.californium:californium-core:1.0.4",
    "org.eclipse.californium:element-connector:1.0.4",
    "org.ehcache:ehcache:3.5.0",
    "org.fusesource.hawtdispatch:hawtdispatch:1.22",
    "org.fusesource.hawtbuf:hawtbuf:1.11",
    "org.fusesource.hawtdispatch:hawtdispatch-transport:1.21",#test
    "org.fusesource.mqtt-client:mqtt-client:1.12",
    "org.influxdb:influxdb-java:2.9",
    "org.lz4:lz4-java:1.4.1",
    "org.mongodb:mongo-java-driver:3.6.3",
    "org.quartz-scheduler:quartz:2.2.2",
    "org.slf4j:jcl-over-slf4j:1.7.25",
    "org.slf4j:jul-to-slf4j:1.7.25",
    "org.slf4j:slf4j-api:1.7.25",
    "org.slf4j:slf4j-ext:1.7.25",

    "org.springframework.boot:spring-boot-configuration-metadata:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-properties-migrator:%s" % _SPRING_BOOT_VER,
    #"org.springframework.boot:spring-boot-starter-web:%s" % _SPRING_BOOT_VER,
    maven.artifact(
            group = "org.springframework.boot",
            artifact = "spring-boot-starter-web",
            version = _SPRING_BOOT_VER,
            exclusions = [
                "org.hibernate.validator:hibernate-validator",
                "org.springframework.boot:spring-boot-autoconfigure",
                "com.fasterxml:classmate",
                "com.fasterxml.jackson.datatype:jackson-datatype-jsr310",
            ]
    ),
    "org.springframework.boot:spring-boot-starter-tomcat:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-starter-websocket:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-starter-json:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-starter-tomcat:%s" % _SPRING_BOOT_VER,
    # == START bazel-springboot-rule defaults ==
    # Spring boot dependencies loaded from bazel-springboot-rule
    "org.springframework.boot:spring-boot:%s" % _SPRING_BOOT_VER,

    "org.springframework.boot:spring-boot-autoconfigure:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-loader:%s" % _SPRING_BOOT_VER,
    "org.springframework.boot:spring-boot-starter-jetty:%s" % _SPRING_BOOT_VER,
    maven.artifact(
            group = "org.springframework.boot",
            artifact = "spring-boot-starter",
            version = _SPRING_BOOT_VER,
            exclusions = [
                "org.springframework.boot:spring-boot-autoconfigure",
            ]
    ),
    "org.springframework.boot:spring-boot-starter-logging:%s" % _SPRING_BOOT_VER,
    #Spring dependencies loaded from bazel-springboot-rule
    "javax.servlet:javax.servlet-api:3.1.0",
    "org.springframework:spring-beans:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-context:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-web:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-core:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-aop:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-expression:%s" % _SPRING_FRM_VER,
    "org.eclipse.jetty:jetty-server:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-servlet:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-util:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-webapp:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-security:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-http:9.4.8.v20171121",
    "org.eclipse.jetty:jetty-io:9.4.8.v20171121",
    # == END bazel-springboot-rule defaults ==
    #"org.springframework:http:%s" % _SPRING_FRM_VER,
    "org.springframework.security:spring-security-config:%s" % _SPRING_FRM_VER,
    "org.springframework.security:spring-security-core:%s" % _SPRING_FRM_VER,
    "org.springframework.security:spring-security-web:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-context-support:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-messaging:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-webmvc:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-websocket:%s" % _SPRING_FRM_VER,
    "org.springframework:spring-jcl:%s" % _SPRING_FRM_VER,
    #"org.springframework:web-client:%s" % _SPRING_FRM_VER,
    "org.xerial.snappy:snappy-java:1.1.7.1",
    "org.yaml:snakeyaml:1.19",
   # WSO2 dependencies.
    "com.google.code.ksoap2-android:ksoap2-android:3.4.0",
    "com.google.code.ksoap2-android:ksoap2-j2se:3.4.0",
    "com.google.code.ksoap2-android:ksoap2-base:3.4.0",
    "org.xmlpull:xmlpull:1.1.3.3",
    # ActiveMQ dependencies
    # "org.apache.activemq.protobuf:activemq-protobuf:1.1", # not needed, all protobuf jars are maven plugins
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-amqp",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-mqtt",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-stomp",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-kahadb-store",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-broker",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    maven.artifact(
                group = "org.apache.activemq",
                artifact = "activemq-client",
                version = "5.14.0",
                exclusions = [
                    "org.apache.activemq.protobuf:activemq-protobuf",
                ]
        ),
    # Azure dependencies.
    "com.microsoft.azure:azure-eventhubs:1.0.0",
    "com.microsoft.azure:azure-eventhubs-eph:1.0.0",
    # RabbitMQ dependencies
    "com.rabbitmq:amqp-client:3.6.0",
    # WebSocket dependencies.
    "javax.websocket:javax.websocket-api:1.1",
    "org.glassfish.tyrus:tyrus-server:1.13.1",
    "org.glassfish.tyrus:tyrus-container-grizzly-server:1.13.1",
    "org.apache.geronimo.specs:geronimo-jms_1.1_spec:1.1.1",
    "com.twilio.sdk:twilio-java-sdk:3.4.5",
    "com.github.kenglxn.qrgen:javase:2.5.0",
    "com.github.kenglxn.qrgen:core:2.5.0",
]

_DEPS_IN_QUESTION = [
    "com.google.guava:guava:26.0-android",
    "com.sitewhere:sitewhere-device-protobuf:2.0.1",
    "org.apache.commons.io:commonsIO:2.5.0",
]

ALL_DEPS = _DEPS # + _DEPS_IN_QUESTION
